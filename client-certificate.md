# Using client certificates with Agunua

## Create the certificate

### RSA

(We could also use two separate commands, one to generate the key and one for the CRT (certificate request).)

(RSA certificates can also be generated from Lagrange, in its Identity menu, and then used by Agunua. They are located in `~/.config/lagrange/idents`.)

```
% openssl req -subj /CN=User -newkey rsa:2048 -sha256 -days 1000 -nodes -keyout key-user.pem -out cert-user.crt
% openssl x509 -in cert-user.crt -out cert-user.pem -req -signkey key-user.pem -days 2001
```

### ECDSA

```
% openssl ecparam -out key-user.pem -name prime256v1 -genkey 
% openssl req -new -subj /CN=User -sha256 -nodes -key key-user.pem -out cert-user.crt
% openssl x509 -days 1000 -in cert-user.crt -out cert-user.pem -req -signkey key-user.pem -days 2001
% cat cert-user.pem key-user.pem > import-with-lagrange.pem
```

## Use the certificate

(Exemple with Station)

```
% agunua --key key-user.pem --certificate cert-user.pem gemini://station.martinrue.com/name/id/post\?"Hello, world"
```

If you like shell functions, you can turn that into a function:

```
poststation () {
    agunua --insecure --key key-user.pem --certificate cert-user.pem gemini://station.martinrue.com/name/id/post\?$1
}
```

Which can then be used with:

```
% poststation "How do you feel?"
```
