#!/usr/bin/env python3

import Agunua

import pytest

# gemini://random-projects.net/torture/index.gemini
# 2022-07-21: test disabled since the server no longer works and
# https://random-projects.net/ contains no way to warn the maintainer.
# The files at this place are mostly to test renderers so they are not really for us.

def test_headers():
    me = Agunua.GeminiUri("gemini://random-projects.net/torture/0001.gemini", get_content=True, insecure=True)
    assert me.status_code == "20" and "### 9" in me.payload 

def test_markdown():
    me = Agunua.GeminiUri("gemini://random-projects.net/torture/0002.md", get_content=True, insecure=True)
    assert me.status_code == "20" and me.mediatype == "text/markdown" and \
        "- [Calculator](https://github.com/MasterQ32/Dunstwolke/tree/master/dunstwolke/examples/calculactor)" in me.payload

def test_html():
    me = Agunua.GeminiUri("gemini://random-projects.net/torture/0003.htm", get_content=True, insecure=True)
    assert me.status_code == "20" and me.mediatype == "text/html" and \
        "<a href=\"https://gemini.circumlunar.space/\">https://gemini.circumlunar.space/</a><br />" in me.payload

def test_links():
    me = Agunua.GeminiUri("gemini://random-projects.net/torture/0004.gemini", parse_content=True,
                          get_content=True, insecure=True)
    assert me.status_code == "20" and me.mediatype == "text/gemini" and \
        me.links == ["gemini://random-projects.net/torture/0004.gemini",
                    "gemini://random-projects.net/torture/0004.gemini",
                    # "gemini://random-projects.net/torture/0004.gemini", # See issue #30
                    "gemini://random-projects.net/torture/0004.gemini",
                    "gemini://gemini.conman.org/sigil"]
    
def test_bmp():
    me = Agunua.GeminiUri("gemini://random-projects.net/torture/0005-bmp.bmp", get_content=True, insecure=True)
    assert me.status_code == "20" and me.mediatype == "image/bmp" # "and
    # me.binary" won't work since Agunua sets binary only when there
    # is a text file.

# Two other image formats.

def test_emoji_text():
    me = Agunua.GeminiUri("gemini://random-projects.net/torture/0006.txt", get_content=True, insecure=True)
    assert me.status_code == "20" and me.mediatype == "text/plain" and not me.binary and "🧛‍♀️ woman vampire" in me.payload 

def test_emoji_markdown():
    me = Agunua.GeminiUri("gemini://random-projects.net/torture/0006.md", get_content=True, insecure=True)
    assert me.status_code == "20" and me.mediatype == "text/markdown" and not me.binary and "👩‍🌾 woman farmer" in me.payload 

def test_preformatted():
    me = Agunua.GeminiUri("gemini://random-projects.net/torture/0007.gemini", parse_content=True,
                          get_content=True, insecure=True)
    assert me.status_code == "20" and me.mediatype == "text/gemini" and not me.binary and len(me.links) == 1

