#!/usr/bin/env python3

import Agunua

import pytest

def test_socks():
    me = Agunua.GeminiUri("gemini://nreqeynb23uo5btcibmpgj3xzrb7rdoe3bojab56golipzysgbbiavid.onion/",
                          insecure=True, use_socks=("localhost", 9050))
    assert me.network_success and me.status_code == "20"
    
def test_wrong_proxy():
    with pytest.raises(Agunua.WrongSocksProxy):
        me = Agunua.GeminiUri("gemini://gemini.bortzmeyer.org/", use_socks=("does-not-exist", 9999))

def test_broken_proxy():
    me = Agunua.GeminiUri("gemini://gemini.bortzmeyer.org/", use_socks=("localhost", 9999))
    assert not me.network_success
    
