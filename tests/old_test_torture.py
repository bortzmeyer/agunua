#!/usr/bin/env python3

# 2021-11-28: now gone :-(
# gemini://gemini.conman.org/test/torture/

import Agunua
import Agunua.status

import pytest

def test_links():
    me = Agunua.GeminiUri("gemini://gemini.conman.org/test/torture/0001", insecure=True, parse_content=True)
    assert me.status_code == "20" and me.links == ["gemini://gemini.conman.org/test/torture/0002"]
    me = Agunua.GeminiUri("gemini://gemini.conman.org/test/torture/0002", insecure=True, parse_content=True)
    assert me.status_code == "20" and me.links == ["gemini://gemini.conman.org/test/torture/0003"]
    me = Agunua.GeminiUri("gemini://gemini.conman.org/test/torture/0003", insecure=True, parse_content=True)
    assert me.status_code == "20" and me.links == ["gemini://gemini.conman.org/test/torture/0004"]
    me = Agunua.GeminiUri("gemini://gemini.conman.org/test/torture/0004", insecure=True, parse_content=True)
    assert me.status_code == "20" and me.links == ["gemini://gemini.conman.org/test/torture/0005"]
    me = Agunua.GeminiUri("gemini://gemini.conman.org/test/torture/0005", insecure=True, parse_content=True)
    assert me.status_code == "20" and me.links == ["gemini://gemini.conman.org/test/torture/0006"]
    me = Agunua.GeminiUri("gemini://gemini.conman.org/test/torture/0006", insecure=True, parse_content=True)
    assert me.status_code == "20" and me.links == ["gemini://gemini.conman.org/test/torture/0007"]
    me = Agunua.GeminiUri("gemini://gemini.conman.org/test/torture/0007", insecure=True, parse_content=True)
    assert me.status_code == "20" and me.links == ["gemini://gemini.conman.org/test/torture/0008"]
    me = Agunua.GeminiUri("gemini://gemini.conman.org/test/torture/0008", insecure=True, parse_content=True)
    assert me.status_code == "20" and me.links == ["gemini://gemini.conman.org/test/torture/0009",
                                                   "gemini://gemini.conman.org/test/torture/0011"]

def test_mime():
    me = Agunua.GeminiUri("gemini://gemini.conman.org/test/torture/0012", insecure=True, get_content=True)
    assert me.mediatype == "text/gemini" and me.charset == "utf-8" and "Ïñtèrñàtìòñálízâtîøñ" in me.payload
    me = Agunua.GeminiUri("gemini://gemini.conman.org/test/torture/0013", insecure=True, get_content=True)
    assert me.mediatype == "text/gemini" and me.charset == "iso-8859-1" and "Ïñtèrñàtìòñálízâtîøñ" in me.payload
    me = Agunua.GeminiUri("gemini://gemini.conman.org/test/torture/0014", insecure=True, get_content=True)
    assert me.mediatype == "text/gemini" and me.charset == "us-ascii" and "Internationalized" in me.payload
    me = Agunua.GeminiUri("gemini://gemini.conman.org/test/torture/0015", insecure=True, get_content=True)
    assert me.mediatype == "text/gemini" and me.charset == "utf-8" # It also has a format parameter but we ignore it
    me = Agunua.GeminiUri("gemini://gemini.conman.org/test/torture/0016", insecure=True, get_content=True)
    assert me.mediatype == "text/gemini" and me.charset == "utf-8" # It also has a foo parameter but we ignore it
    me = Agunua.GeminiUri("gemini://gemini.conman.org/test/torture/0017", insecure=True, get_content=True)
    assert me.mediatype == "text/gemini" and me.charset == "utf-8" # charset is at the end but it shouldn't matter
    me = Agunua.GeminiUri("gemini://gemini.conman.org/test/torture/0018", insecure=True, get_content=True)
    assert me.mediatype == "text/gemini" and me.charset == "utf-8" # charset is in upper-case but it shouldn't matter
    me = Agunua.GeminiUri("gemini://gemini.conman.org/test/torture/0019", insecure=True, get_content=True)
    assert me.mediatype == "text/gemini" and me.charset == "utf-8" # charset *value* is in upper-case but it shouldn't matter
    
def test_non_gemini():
    me = Agunua.GeminiUri("gemini://gemini.conman.org/test/torture/0019", insecure=True, parse_content=True)
    assert me.links == ["gemini://gemini.conman.org/test/torture/0020"] # We ignore non-Gemini links
    me = Agunua.GeminiUri("gemini://gemini.conman.org/test/torture/0020", insecure=True, parse_content=True)
    assert me.links == ["gemini://gemini.conman.org/test/torture/0021"] # We ignore non-Gemini links
    me = Agunua.GeminiUri("gemini://gemini.conman.org/test/torture/0021", insecure=True, parse_content=True)
    assert me.links == ["gemini://gemini.conman.org/test/torture/0022"] # We ignore non-Gemini links

def test_redirect():
     me = Agunua.GeminiUri("gemini://gemini.conman.org/test/redirhell/", follow_redirect=True, insecure=True)
     assert not me.network_success
     me = Agunua.GeminiUri("gemini://gemini.conman.org/test/redirhell2/", follow_redirect=True, insecure=True)
     assert not me.network_success
     me = Agunua.GeminiUri("gemini://gemini.conman.org/test/redirhell3/", follow_redirect=True, insecure=True)
     assert not me.network_success
     me = Agunua.GeminiUri("gemini://gemini.conman.org/test/redirhell4", follow_redirect=True, insecure=True)
     assert not me.network_success
     me = Agunua.GeminiUri("gemini://gemini.conman.org/test/redirhell5", follow_redirect=True, insecure=True)
     assert not me.network_success
     with pytest.raises(Agunua.NonGeminiUri): # gemini://gemini.conman.org/test/torture/0027
         me = Agunua.GeminiUri("gemini://gemini.conman.org/test/redirhell6", follow_redirect=True, insecure=True)

def test_bad_links():
    me = Agunua.GeminiUri("gemini://gemini.conman.org/test/torture/0028", insecure=True, parse_content=True)
    assert me.links == ["gemini://gemini.conman.org/test/torture/This", # The link is broken but we have no way to know
                        "gemini://gemini.conman.org/test/torture/0029"]
    me = Agunua.GeminiUri("gemini://gemini.conman.org/test/torture/0029", insecure=True, parse_content=True)
    assert me.links == ["gemini://gemini.conman.org/test/torture/This", # The link is broken but we have no way to know
                        "gemini://gemini.conman.org/test/torture/0030"]
    me = Agunua.GeminiUri("gemini://gemini.conman.org/test/torture/0030", insecure=True, parse_content=True)
    assert me.links == ["gemini://gemini.conman.org/test/torture/This", # The link is broken but we have no way to know
                        "gemini://gemini.conman.org/test/torture/0031"]
    me = Agunua.GeminiUri("gemini://gemini.conman.org/test/torture/0031", insecure=True, parse_content=True)
    assert me.links == ["gemini://gemini.conman.org/test/torture/<0032>", # Illegal characters but are we supposed to do something?
                        "gemini://gemini.conman.org/test/torture/0032"]
    me = Agunua.GeminiUri("gemini://gemini.conman.org/test/torture/0032", insecure=True, parse_content=True)
    assert me.links == ["gemini://gemini.conman.org/test/torture/0033"] # Link with no content, must be ignored
    me = Agunua.GeminiUri("gemini://gemini.conman.org/test/torture/0033", insecure=True, parse_content=True)
    assert me.links == ["gemini://gemini.conman.org/test/torture/0034"]

def test_status_code():
    me = Agunua.GeminiUri("gemini://gemini.conman.org/test/torture/0034a", insecure=True)
    assert me.status_code[0] not in Agunua.status.categories
    me = Agunua.GeminiUri("gemini://gemini.conman.org/test/torture/0035a", insecure=True)
    assert me.status_code not in Agunua.status.codes and me.status_code[0] in Agunua.status.categories
    me = Agunua.GeminiUri("gemini://gemini.conman.org/test/torture/0036a", follow_redirect = True, insecure=True)
    assert me.status_code not in Agunua.status.codes and me.status_code[0] in Agunua.status.categories
    me = Agunua.GeminiUri("gemini://gemini.conman.org/test/torture/0037a", follow_redirect = True, insecure=True)
    assert me.status_code not in Agunua.status.codes and me.meta == "SNAFU"
    me = Agunua.GeminiUri("gemini://gemini.conman.org/test/torture/0038a", follow_redirect = True, insecure=True)
    assert me.status_code not in Agunua.status.codes and me.meta == "FUBAR"
    me = Agunua.GeminiUri("gemini://gemini.conman.org/test/torture/0039a", follow_redirect = True, insecure=True)
    assert not me.network_success # Invalid status code
    me = Agunua.GeminiUri("gemini://gemini.conman.org/test/torture/0040a", follow_redirect = True, insecure=True)
    assert not me.network_success # Invalid header line
